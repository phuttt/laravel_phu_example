<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Laravel PHP Framework</title>
        <style>
            #@import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

            body {
                margin:0;
                font-family:'Lato', sans-serif;
                text-align:center;
                color: #999;
            }

            .welcome {
                width: 300px;
                height: 300px;
                position: absolute;
                left: 50%;
                top: 50%; 
                margin-left: -150px;
                margin-top: -150px;
            }

            a, a:visited {
                color:#FF5949;
                text-decoration:none;
            }

            a:hover {
                text-decoration:underline;
            }

            ul li {
                display:inline;
                margin:0 1.2em;
            }

            p {
                margin:2em 0;
                color:#555;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>This is demo first app on laravel</h1>

            <table cellspacing="0" cellpadding="0" width="100%" border="1">
                <tr>
                    <td>Total Item: <?php echo $total; ?></td>
                    <td colspan="4">Item Per Page: <?php echo $itemPer; ?></td>                
                </tr>
                <tr>
                    <td>id</td>
                    <td>name</td>
                    <td>bio</td>
                    <td>created</td>
                    <td>update</td>
                </tr>
                <?php
                foreach ($infos as $info) {
                    echo "<tr><td>";
                    echo $info->id_author;
                    echo "</td><td>";
                    echo $info->name;
                    echo "</td><td>";
                    echo $info->bio;
                    echo "</td><td>";
                    echo $info->created_at;
                    echo "</td><td>";
                    echo $info->updated_at;
                    echo "</td></tr>";
                }
                ?>
            </table>
        </div>
        <div class="pagination">
            <?php echo $infos->links(); ?>
        </div>
    </body>
</html>
