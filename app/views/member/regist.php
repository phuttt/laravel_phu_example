<style>
    
    div#page_content{
        width:800px;
        text-align: center
    }
   
    div.link_add_new{
        text-align: center;
    }
    .page_content > div{
        text-align: center;
    }
    span.error{
        color:red;
    }
    form input[type=text]{
        width:250px;
    }
</style>
<div class="page_content">
    <div>

    <h2>Register!</h2>
    
    <?php echo Form::open(array('url' => 'member/regist', 'method' => 'post')); ?>
    <table align='center'>
        <tr>
            <td><?php echo Form::label('name', 'Username')?></td>
            <td><?php echo Form::text('name', Input::old('name')) ?><br> <span class='error'><?php echo $errors->first('name') ?> </span></td>
        </tr>
        <tr>
            <td><?php echo Form::label('address', 'Address')?></td>
            <td><?php echo Form::text('address', Input::old('address')) ?></td>
        </tr>
        <tr>
            <td><?php echo Form::label('email', 'E-mail') ?></td>
            <td><?php echo Form::text('email', Input::old('email')) ?><br> <span class='error'><?php echo $errors->first('email') ?></span></td>
        </tr>
        <tr>
            <td colspan='2' align='right'><?php echo Form::submit('Register!') ?></td>
        </tr>
    </table>
    <?php echo Form::close() // Form::token() . Form::close() ?>
        
    </div>
    <div class="link_add_new">
            <a href="list"  >back to list  </a>
        </div>
</div>

