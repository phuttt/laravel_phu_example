<style>
    table td.order{
        text-align: center;
    }
    table{
        border-collapse: collapse;
    border-spacing: 0px;
    }
    div#page_content{
        width:800px;
        text-align: center
    }
    tr:nth-child(even) {background: #CCC}
    tr:nth-child(odd) {background: #FFF}
    tr.header{
        background: none;
    }
    table{
        width:600px;
    }
    div.link_add_new{
        text-align: center;
    }
    .list td:not(.order){
        padding-left: 5px;
    }

</style>
<div class="page_content">
    <div>
        <table cellspacing="0" cellpadding="0"  border="1" align="center" class='list'>
            <tr class='header'>
                <th colspan="4" >Total Item: <?php echo $total; ?></th>
            </tr>
            <tr class='header'>
                <th>id</th>
                <th>Name</th>
                <th>Address</th>
                <th>Email</th>
            </tr>
            <?php $index = 1; ?>
            <?php
            foreach ($infos as $info) {
                echo "<tr><td class='order'>";
                echo $index;
                echo "</td><td>";
                echo $info->name;
                echo "</td><td>";
                echo $info->address;
                echo "</td><td>";
                echo $info->email;
                echo "</td></tr>";
                
                $index = $index + 1;
            }
            ?>
        </table>
        <div class="link_add_new">
            <a href="regist"  >regist new member </a>
        </div>
        
    </div>
</div>

