<?php



use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TestCommand extends Command {

/**
 * The console command name.
 *
 * @var string
 */
protected $name = 'command:test';

/**
 * The console command description.
 *
 * @var string
 */
protected $description = 'Command calculate toltal rows of a table';

/**
 * Create a new command instance.
 *
 * @return void
 */
public function __construct()
{
parent::__construct();
}

/**
 * Execute the console command.
 *
 * @return void
 */
public function fire()
{
     $table_name = $this->argument("table");
     $total_row = DB::table('member_info')->count();
     
      $this->line("Total rows of table $table_name are " . $total_row ) ;
}

/**
 * Get the console command arguments.
 *
 * @return array
 */
protected function getArguments()
{
return array(
array('table', InputArgument::REQUIRED, 'Name of table which calculate total rows.'),
);
}

/**
 * Get the console command options.
 *
 * @return array
 */
protected function getOptions() {
        return array();
    }

}